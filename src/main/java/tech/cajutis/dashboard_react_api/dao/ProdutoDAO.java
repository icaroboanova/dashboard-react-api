package tech.cajutis.dashboard_react_api.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.google.gson.JsonObject;

import tech.cajutis.dashboard_react_api.model.Produto;

public class ProdutoDAO {

	private EntityManager entityManager;

	public ProdutoDAO() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("Dashboard-React");

		this.entityManager = factory.createEntityManager();
	}
	
	public List<Produto> getProdutos(){
		String query = "select p from Produto p";
		TypedQuery<Produto> tq = entityManager.createQuery(query, Produto.class);
		return tq.getResultList();
	}
	
	public Produto getProdutoById(int id) {
		return entityManager.find(Produto.class, id);
	}
	
	public void createProduto(Produto produto) {
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(produto);
		this.entityManager.getTransaction().commit();
	}
	
	public void updateProduto(int id, JsonObject body) {
		this.entityManager.getTransaction().begin();
		
		Produto produto = this.getProdutoById(id);
		if(!body.get("nome").getAsString().equals("")) produto.setNome(body.get("nome").getAsString());
		produto.setValor(body.get("valor").getAsFloat());
		
		this.entityManager.getTransaction().commit();
	}
	
	public void deleteProduto(int id) {
		Produto produto = this.getProdutoById(id);
		this.entityManager.getTransaction().begin();
		this.entityManager.remove(produto);
		this.entityManager.getTransaction().commit();
	}
}
