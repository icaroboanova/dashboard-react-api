package tech.cajutis.dashboard_react_api.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.google.gson.JsonObject;

import tech.cajutis.dashboard_react_api.model.Cliente;

public class ClienteDAO {

	private EntityManager entityManager;

	public ClienteDAO() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("Dashboard-React");

		this.entityManager = factory.createEntityManager();
	}

	public List<Cliente> getClientes(){
		String query = "select c from Cliente c";
		TypedQuery<Cliente> tq = entityManager.createQuery(query, Cliente.class);
		return tq.getResultList();
	}

	public Cliente getClienteById(int id) {
		return entityManager.find(Cliente.class, id);
	}
	
	public boolean createCliente(Cliente cliente) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(cliente);
			entityManager.getTransaction().commit();
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public void updateCliente(int id, JsonObject body) {
			entityManager.getTransaction().begin();
			
			Cliente cliente = this.getClienteById(id);
			
			if(!body.get("nome").getAsString().equals("")) {
				cliente.setNome(body.get("nome").getAsString());
			}
			entityManager.getTransaction().commit();
	}
	
	public void deleteCliente(int id) {
			entityManager.getTransaction().begin();
			Cliente cliente = this.getClienteById(id);
			entityManager.remove(cliente);
			entityManager.getTransaction().commit();
	}

}
