package tech.cajutis.dashboard_react_api.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import jdk.internal.org.jline.reader.Parser;
import tech.cajutis.dashboard_react_api.dao.ClienteDAO;
import tech.cajutis.dashboard_react_api.dao.ProdutoDAO;
import tech.cajutis.dashboard_react_api.dao.VendaDAO;
import tech.cajutis.dashboard_react_api.model.Cliente;
import tech.cajutis.dashboard_react_api.model.Produto;
import tech.cajutis.dashboard_react_api.model.Venda;
import tech.cajutis.dashboard_react_api.utils.Utils;

@Path("")
public class Controller {
	
	private final static ClienteDAO CLIENTE = new ClienteDAO();
	private final static ProdutoDAO PRODUTO = new ProdutoDAO();
	private final static VendaDAO VENDA = new VendaDAO();

	@GET
	@Path("/hello")
	public Response helloWorld() {
		return Response.ok()//200
				.entity("Hello World!")
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.build();
	}

	//operações cliente

	@GET
	@Path("/clientes")
	public Response getClientes() {
		List<Cliente> lista = CLIENTE.getClientes();
		JsonArray resposta = new JsonArray();
		lista.forEach(c -> {
			JsonObject cliente = new JsonObject();
			cliente.addProperty("id", c.getId());
			cliente.addProperty("nome", c.getNome());
			resposta.add(cliente);
		});

		return Response.ok()//200
				.entity(resposta.toString())
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.build();
	}

	@GET
	@Path("/cliente")
	public Response getCliente(@DefaultValue("1") @QueryParam("id") int id) {
		Cliente cliente = CLIENTE.getClienteById(id);

		JsonObject resposta = new JsonObject();
		resposta.addProperty("id", cliente.getId());
		resposta.addProperty("nome", cliente.getNome());

		return Response.ok()//200
				.entity(resposta.toString())
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.build();
	}

	@POST
	@Path("/cliente")
	public Response createCliente(String data) {
		try {
			JsonObject body = Utils.getBody(data);

			Cliente cliente = new Cliente(body.get("nome").getAsString());
			CLIENTE.createCliente(cliente);

			return Response.ok()//200
					.entity("true")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		} catch (IOException e) {
			return Response.ok()//200
					.entity("false")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		}
	}

	@POST
	@Path("/cliente/update/{id}")
	public Response updateCliente(@PathParam("id") int id, String data) {
		try {
			JsonObject body = Utils.getBody(data);
			
			CLIENTE.updateCliente(id, body);

			return Response.ok()//200
					.entity("true")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		} catch (IOException e) {
			return Response.ok()//200
					.entity("false")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		}
	}

	@POST
	@Path("/cliente/delete/{id}")
	public Response deleteCliente(@PathParam("id") int id) {
		
		CLIENTE.deleteCliente(id);

		return Response.ok()//200
				.entity("true")
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.build();

	}

	//operações produto

	@GET
	@Path("/produtos")
	public Response getProdutos() {
		List<Produto> lista = PRODUTO.getProdutos();
		JsonArray resposta = new JsonArray();
		lista.forEach(p -> {
			JsonObject produto = new JsonObject();
			produto.addProperty("id", p.getId());
			produto.addProperty("nome", p.getNome());
			produto.addProperty("valor", p.getValor());
			resposta.add(produto);
		});

		return Response.ok()//200
				.entity(resposta.toString())
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.build();
	}
	
	@POST
	@Path("/produto")
	public Response createProduto(String data) {
		try {
			JsonObject body = Utils.getBody(data);
			
			Produto produto = new Produto(body.get("nome").getAsString(), body.get("valor").getAsFloat());
			PRODUTO.createProduto(produto);
			
			return Response.ok()//200
					.entity("true")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		} catch (IOException e) {
			return Response.ok()//200
					.entity("false")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		}
	}
	
	@POST
	@Path("/produto/update/{id}")
	public Response updateProduto(@PathParam("id") int id, String data) {
		try {
			JsonObject body = Utils.getBody(data);

			PRODUTO.updateProduto(id, body);

			return Response.ok()//200
					.entity("true")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		} catch (IOException e) {
			return Response.ok()//200
					.entity("false")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		}
	}

	@POST
	@Path("/produto/delete/{id}")
	public Response deleteProduto(@PathParam("id") int id) {
		PRODUTO.deleteProduto(id);

		return Response.ok()//200
				.entity("true")
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.build();

	}

	//operações vendas
	@GET
	@Path("/vendas")
	public Response getVendas() {
		List<Venda> lista = VENDA.getVendas();
		JsonArray resposta = new JsonArray();
		lista.forEach(v -> {
			JsonObject venda = new JsonObject();
			venda.addProperty("id", v.getId());
			venda.addProperty("nome_cliente", v.getCliente().getNome());
			venda.addProperty("nome_produto", v.getProduto().getNome());
			venda.addProperty("quantidade", v.getQuantidade());
			venda.addProperty("data", v.getData().toLocaleString());
			venda.addProperty("valor", v.getQuantidade() * v.getProduto().getValor());
			resposta.add(venda);
		});

		return Response.ok()//200
				.entity(resposta.toString())
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.build();
	}

	@POST
	@Path("/venda")
	public Response createVenda(String data) {

		try {
			JsonObject body = Utils.getBody(data);

			JsonParser parse = new JsonParser();

			Cliente cliente = CLIENTE.getClienteById(body.get("cliente").getAsInt());
			JsonArray produtosVendidos = parse.parse(body.get("produtosVendidos").getAsString()).getAsJsonArray();
			produtosVendidos.forEach(p -> {
				Produto produto = PRODUTO.getProdutoById(p.getAsJsonObject().get("id").getAsInt());
				Venda venda = new Venda(cliente, produto, p.getAsJsonObject().get("quantidade").getAsInt(), new Date());
				boolean criar = VENDA.createVenda(venda);
			});


			return Response.ok()//200
					.entity("true")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.header("Content-Type", "application/json")
					.build();
		} catch (IOException e) {
			return Response.ok()//200
					.entity("false")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.header("Content-Type", "application/json")
					.build();
		}


	}
	
	@POST
	@Path("/venda/update/{id}")
	public Response updateVenda(@PathParam("id") int id, String data) {
		try {
			JsonObject body = Utils.getBody(data);

			VENDA.updateVenda(id, body);

			return Response.ok()//200
					.entity("true")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		} catch (IOException e) {
			return Response.ok()//200
					.entity("false")
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
					.build();
		}
	}

	@POST
	@Path("/venda/delete/{id}")
	public Response deleteVenda(@PathParam("id") int id) {
		VENDA.deleteVenda(id);

		return Response.ok()//200
				.entity("true")
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.build();

	}

}
