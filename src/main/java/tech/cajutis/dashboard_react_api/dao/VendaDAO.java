package tech.cajutis.dashboard_react_api.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.google.gson.JsonObject;

import tech.cajutis.dashboard_react_api.model.Venda;

public class VendaDAO {

	private EntityManager entityManager;

	public VendaDAO() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("Dashboard-React");

		this.entityManager = factory.createEntityManager();
	}

	public List<Venda> getVendas(){
		String query = "select v from Venda v";
		TypedQuery<Venda> tq = entityManager.createQuery(query, Venda.class);
		return tq.getResultList();
	}
	
	public Venda getVenda(int id) {
		return entityManager.find(Venda.class, id);
	}

	public boolean createVenda(Venda venda) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(venda);
			entityManager.getTransaction().commit();
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public void updateVenda(int id, JsonObject body) {
		entityManager.getTransaction().begin();
		Venda venda = this.getVenda(id);
		
		venda.setQuantidade(body.get("quantidade").getAsInt());
		
		entityManager.getTransaction().commit();
	}
	
	public void deleteVenda(int id) {
		Venda venda = this.getVenda(id);
		entityManager.getTransaction().begin();
		entityManager.remove(venda);
		entityManager.getTransaction().commit();
	}
}
